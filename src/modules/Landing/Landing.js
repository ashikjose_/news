import React, { Component } from 'react';
import './Landing.css';
import moment from 'moment'
import labels from '../../constants/labels/main';
import Navbar from '../../components/Navbar';
import Mag from '../../components/Mag';
import { stringShorten } from '../../helpers/string.js';
import Footer from '../../components/Footer';

class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: moment().format('MMMM Do, YYYY'),
      metaData: [2,4,1],
      newsData: [],
      imageData: []
    }
  }

  getLabel() {
    return labels.getLabels('en')
  }

  componentWillMount() {
    const NewsAPI = require('newsapi');
    const newsapi = new NewsAPI('3cc86b3052dc4e6390b15af3369a7647');
    newsapi.v2.topHeadlines({
      sources: 'the-verge'
    }).then(response => {
        this.setState({
          newsData: response.articles,
          imageData: response.articles.slice(3,9)
        })
    });
  }

  render() {
    return (
      <div>
        <Navbar />
        <div className="container my-container">
          <div className="row">
            
            {
              this.state.imageData.map((data, index) => {
                //<div className={`col-sm-${val}`}>
                  return (
                    <Mag newsData={data} key={index}/>
                  )
                }
              )
            }
          </div>
        </div>

        <div className="container my-container-small">
          <div className="row">
            
            {
              this.state.newsData.map((data, index) => {
                //<div className={`col-sm-${val}`}>
                  return (
                    <div className="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" key={index}>
                      <div className="bordered-box">
                        <div className="container-fluid">
                          <div className="row">
                            <div className="col-5 boom">
                              <div className="image-container">
                                <img src={data.urlToImage} width="200px" height="200px" alt={data.author} className="image-height"/>
                              </div>
                            </div>
                            <div className="col-12 col-xs-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                              <div className="text-box">
                                <h3>{data.title}</h3>
                                <h6>{stringShorten(data.description, 300)}</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                }
              )
            }
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}

export default Landing;
