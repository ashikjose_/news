const labels = {
  en: {
    KOLONY: 'Kolony',
    FEED: 'The Rift',
    A: 'A',
    PARENT: 'Random Media Company',
    PRODUCTION: 'Production'
  },
  fr: {
    KOLONY: 'Kolonie',
    FEED: 'The Rift'
  }
}

const getLabels = (lang) => {
  return labels[lang]
}

export default {
  getLabels
}