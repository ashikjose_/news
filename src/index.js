import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import Landing from '../src/modules/Landing';
import * as serviceWorker from './serviceWorker';
import WebFont from 'webfontloader';

ReactDOM.render(<Landing />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

WebFont.load({
  google: {
    families: ['El Messiri:300,400,700', 'sans-serif', 'Nunito:400,700', 'sans-serif', 'Fredoka One', 'Trykker:400', 'serif']
  }
});
