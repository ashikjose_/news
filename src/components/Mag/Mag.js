import React, { Component } from 'react';
import './Mag.css';
import labels from '../../constants/labels/main';
import { stringShorten } from '../../helpers/string.js';

class Mag extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  getLabel() {
    return labels.getLabels('en')
  }

  renderboom(data) {
    console.log(data)
  }

  render() {
    return (
      <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
        <div className="height image-container" onClick={() =>  this.renderboom(this.props.newsData) }>
          <img src={this.props.newsData.urlToImage} alt={this.props.newsData.title} className="image-height"/>
          <div className="image-text font-32">{ stringShorten(this.props.newsData.title, 40) }</div>
        </div>
      </div>
    );
  }
}

export default Mag;
