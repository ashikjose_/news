import React, { Component } from 'react';
import './Navbar.css';
import moment from 'moment'
import labels from '../../constants/labels/main';

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: moment().format('MMMM Do, YYYY')
    }
  }

  getLabel() {
    return labels.getLabels('en')
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="column navbar align-content-center align-items-center">
          <div className="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h1 className="logo">{ this.getLabel().FEED }</h1>
          </div>
          <div className="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <p className="desc">{ this.state.date }</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Navbar;
