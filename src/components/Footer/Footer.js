import React, { Component } from 'react';
import './Footer.css';
import moment from 'moment'
import labels from '../../constants/labels/main';

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      year: moment().format('YYYY')
    }
  }

  getLabel() {
    return labels.getLabels('en')
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="column footer align-content-center align-items-center">
          <div className="col-sm-12 col-md-12 col-lg-12 col-xl-12 center">
            <h6 className="text">{ this.getLabel().A } { this.getLabel().PARENT } { this.getLabel().PRODUCTION }</h6>
            <p className="text">Duh!, {this.state.year}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
